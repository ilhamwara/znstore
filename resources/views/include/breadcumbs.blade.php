@if(@isset($breadcrumbs))
	<ol class="breadcrumb">
		@foreach ($breadcrumbs as $breadcrumb)
		<li>
			@if(isset($breadcrumb['link']))
				<a href="{{ $breadcrumb['link'] == 'javascript:void(0)' ? $breadcrumb['link']:url($breadcrumb['link']) }}">
            @endif
            	@if(isset($breadcrumb['icon']))
            		<i class="{{$breadcrumb['icon']}}"></i>
            	@endif
            	{{$breadcrumb['name']}}
            @if(isset($breadcrumb['link']))
                </a>
			@endif
		</li>
		@endforeach
	</ol>
@endisset
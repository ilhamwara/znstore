<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left info">
          <p>{{Auth::user()->name}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
        <br>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active"><a href="{{url('backend/dashboard')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        <li><a href="#"><i class="fa fa-list"></i> <span>Category</span></a></li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-list"></i> <span>Products</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> All Products</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Arrival</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Best Seller</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Orders</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> All Order</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Pending Payment</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Success Order</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Cancel Order</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i> <span>Members</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> List Member</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Member Address</a></li>
          </ul>
        </li>
        <li><a href="#"><i class="fa fa-dollar"></i> <span>Payment Setting</span></a></li>
        <li><a href="#"><i class="fa fa-truck"></i> <span>Kurir</span></a></li>
        <li><a href="#"><i class="fa fa-star"></i> <span>Promo</span></a></li>
        <li><a href="#"><i class="fa fa-star"></i> <span>Voucher</span></a></li>
        <li><a href="#"><i class="fa fa-cog"></i> <span>Settings</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
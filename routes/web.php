<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Auth::routes(['verify' => true]);

//MEMBERS
Route::group(['middleware' => ['session','verify']], function () {
	Route::get('/home', 'HomeController@index')->name('home');
});

Route::namespace('Backend')->prefix('backend')->name('backend.')->group(function () {

	Route::get('/', 'Auth\LoginController@index')->name('login');
	Route::post('/login', 'Auth\LoginController@post_login')->name('post_login');

	Route::middleware(['admin'])->group(function () {

		Route::get('/dashboard', 'HomeController@index')->name('dashboard');

	});
});

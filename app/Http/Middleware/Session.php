<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Session
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return redirect('/login')->with('warning','Maaf silahkan login terlebih dahulu');
        }
        return $next($request);
    }
}

<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $page = 'Dasboard';
        $breadcrumbs = [['name' => "Dashboard",'icon' => "fa fa-dashboard"]];
    	return view('backend.home.index',compact('page','breadcrumbs'));
    }
}

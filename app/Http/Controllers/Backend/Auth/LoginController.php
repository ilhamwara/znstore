<?php

namespace App\Http\Controllers\Backend\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use Validator;
use Hash;
use Auth;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/backend/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth')->except('logout');
    // }
    public function index()
    {
        return view('backend.auth.login');
    }
    public function post_login(Request $request)
    {
        $v = Validator::make($request->all(), [
            'email'     => 'required',
            'password'  => 'required'
        ]);
        if ($v->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($v->errors());
        }

        if ($lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        $credentials  = array('email' => $request->email, 'password' => $request->password);

        $user = User::where('email', $credentials['email'])->first();

        if ($user && Hash::check($credentials['password'], $user->password)) {
            if($user['email_verified_at']){
                Auth::login($user);
                return $this->handleUserWasAuthenticated($request, true);
            }else{
                return redirect('/backend')->with('error','Akun anda belum di aktifkan, aktivasi akun silakan');
            }
        }

        if (!$lockedOut) {
            $this->incrementLoginAttempts($request);
        }

        return $this->sendFailedLoginResponse($request);
    }
    protected function handleUserWasAuthenticated(Request $request, $throttles)
    {   
        if ($throttles) {
            $this->clearLoginAttempts($request);
        }
        return redirect('/backend/dashboard');
    }

    // public function logout(Request $request)
    // {
    //     $backend = Auth::user();
    //     if ($backend) {
    //       Auth::logout();
    //       return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/backoffice');
    //     }
    // }
}

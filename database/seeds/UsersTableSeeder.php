<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
	        		[
	        			'name' 				=> 'Administrator',
	        			'type' 				=> 1,
	        			'email' 			=> 'admin@gmail.com',
	        			'email_verified_at' => date('Y-m-d H:i:s'),
	        			'password' 			=> bcrypt('password'),
	        		],[
	        			'name' 				=> 'Member',
	        			'type' 				=> 2,
	        			'email' 			=> 'ilhamwara@gmail.com',
	        			'email_verified_at' => date('Y-m-d H:i:s'),
	        			'password' 			=> bcrypt('password'),
	        		],
        		];

        foreach ($data as $key => $value) {
        	$insert = \App\User::create($value);
        }
    }
}
